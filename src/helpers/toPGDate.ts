import dayjs from 'dayjs';

const toPGDate = (date: string, add: number = 0) => {
  return dayjs(date).add(add, 'day').format('YYYY.MM.DD')
}

export default toPGDate;
