import './db/createDefaultRecords';
import express from 'express';
import router from './api';

const server = express();

server.use(express.json());

server.use('/api', router);

server.listen(3000, () => console.log('listen on 3000'));
