import dayjs from "dayjs";
import { NextFunction, Request, Response } from "express";
import client from "../../db";
import toPGDate from "../../helpers/toPGDate";

export const getCars = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { rows: cars } = await client.query(' \
      SELECT * FROM cars \
      ORDER BY id ASC \
    ')
  
    res.json(cars);
  } catch (e) {
    next(e);
  }
}

export const getCarsUsage = async (req: Request, res: Response, next: NextFunction) => {
  const { startDate, endDate } = req.query;

  if (typeof startDate !== 'string') {
    res.status(400).send('Неверный startDate');
    return;
  }

  if (typeof endDate !== 'string') {
    res.status(400).send('Неверный endDate');
    return;
  }

  try {
    const [{ rows: cars }, { rows: orders }] = await Promise.all([
      client.query(' \
        SELECT * FROM cars \
        ORDER BY id ASC \
      '),
      client.query(`\
        SELECT * FROM orders\
        WHERE startDate between '${toPGDate(startDate)}' and '${toPGDate(endDate)}' \
          OR endDate between '${toPGDate(startDate)}' and '${toPGDate(endDate)}' \
        ORDER BY startDate ASC \
      `)
    ])

    const orderedDaysByCar: Record<string, number> = {}
    const priceSumByCar: Record<string, number> = {}

    cars.forEach(car => {
      orderedDaysByCar[car.id] = 0;
      priceSumByCar[car.id] = 0;
    })

    orders.forEach(order => {
      const newOrder = order;

      if (dayjs(order.startdate).isBefore(startDate)) {
        newOrder.startdate = startDate
      }

      if (dayjs(order.enddate).isAfter(endDate)) {
        newOrder.enddate = endDate
      }

      orderedDaysByCar[newOrder.car] += dayjs(newOrder.enddate).diff(newOrder.startdate, 'day') + 1;
      priceSumByCar[newOrder.car] += newOrder.price
    })
  
    res.json(cars.map(car => ({ 
      ...car, 
      orderedDays: orderedDaysByCar[car.id], 
      usagePercent: Number(orderedDaysByCar[car.id] / (dayjs(endDate).diff(startDate, 'day') + 1) * 100).toFixed(2), 
      priceSum: priceSumByCar[car.id] 
    })));
  } catch (e) {
    next(e);
  }
}