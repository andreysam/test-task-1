import { Router } from 'express';
import { getCars, getCarsUsage } from './controller';

const carsRouter = Router();

carsRouter.get('/', getCars);
carsRouter.get('/usage', getCarsUsage);

export default carsRouter;
