export const calculatePrice = (days: number): number => {
  let price = days * 1000;
  
  if (days >= 3 && days <= 5) {
    return price * 0.95
  }

  if (days >= 6 && days <= 14) {
    return price * 0.90
  }

  if (days >= 15 && days <= 30) {
    return price * 0.85
  }

  return price
};
