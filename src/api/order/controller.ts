import { NextFunction, Request, Response } from "express";
import dayjs from 'dayjs';
import weekday from 'dayjs/plugin/weekday';
import ru from 'dayjs/locale/ru';
import client from "../../db";
import { calculatePrice } from "./services";
import toPGDate from "../../helpers/toPGDate";

dayjs.extend(weekday);

export const getOrders = async (req: Request, res: Response, next: NextFunction) => {
  const { car } = req.query;

  try {
    const { rows: orders } = await client.query(`\
      SELECT startDate, endDate FROM orders\
      WHERE car = '${car}' \
      ORDER BY startDate ASC \
    `)
  
    res.json(orders);
  } catch (e) {
    next(e);
  }
}

export const calculateOrderDetails = async (req: Request, res: Response, next: NextFunction) => {
  const { startDate, endDate, car } = req.query;

  if (typeof startDate !== 'string') {
    res.status(400).send('Неверный startDate');
    return;
  }

  if (typeof endDate !== 'string') {
    res.status(400).send('Неверный endDate');
    return;
  }

  if (typeof car !== 'string') {
    res.status(400).send('Не указан car');
    return;
  }

  if (dayjs(startDate).isAfter(endDate, 'day')) {
    res.status(400).send('startDate должен быть раньше endDate')
    return;
  }

  if (dayjs(startDate).isBefore(new Date(), 'day')) {
    res.status(400).send('startDate не может быть в прошлом')
    return;
  }

  if ([5,6].includes(dayjs(startDate).locale(ru).weekday()) || [5,6].includes(dayjs(endDate).locale(ru).weekday())) {
    res.status(400).send('startDate и endDate не может выпадать на выходной день')
    return;
  }

  try {
    const orders = await client.query(`\
      SELECT COUNT(*) FROM orders\
      WHERE car = '${car}' \
        AND ( \
          startDate between '${toPGDate(startDate, -3)}' and '${toPGDate(endDate, 3)}' \
          OR endDate between '${toPGDate(startDate, -3)}' and '${toPGDate(endDate, 3)}' \
        ) \
    `)

    if (Number(orders.rows[0].count)) {
      res.status(400).send('Бронирование невозможно, существует другой заказ на этот период');
      return;
    }

    const orderDurationInDays = dayjs(endDate).diff(startDate, 'day') + 1;
  
    if (orderDurationInDays > 30) {
      res.status(400).send('Бронирование невозможно, слишком длинный период. Максимум 30 дней')
    }

    res.json(calculatePrice(orderDurationInDays))
  } catch (e) {
    next(e);
  }
}

export const createOrder = async (req: Request, res: Response, next: NextFunction) => {
  const { startDate, endDate, car } = req.body;

  if (typeof startDate !== 'string') {
    res.status(400).send('Неверный startDate');
    return;
  }

  if (typeof endDate !== 'string') {
    res.status(400).send('Неверный endDate');
    return;
  }

  if (typeof car !== 'string') {
    res.status(400).send('Не указан car');
    return;
  }

  if (dayjs(startDate).isAfter(endDate, 'day')) {
    res.status(400).send('startDate должен быть раньше endDate')
    return;
  }

  if (dayjs(startDate).isBefore(new Date(), 'day')) {
    res.status(400).send('startDate не может быть в прошлом')
    return;
  }

  if ([5,6].includes(dayjs(startDate).locale(ru).weekday()) || [5,6].includes(dayjs(endDate).locale(ru).weekday())) {
    res.status(400).send('startDate и endDate не может выпадать на выходной день')
    return;
  }

  try {
    const orders = await client.query(`\
      SELECT COUNT(*) FROM orders\
      WHERE car = '${car}' \
        AND ( \
          startDate between '${toPGDate(startDate, -3)}' and '${toPGDate(endDate, 3)}' \
          OR endDate between '${toPGDate(startDate, -3)}' and '${toPGDate(endDate, 3)}' \
        ) \
    `)

    if (Number(orders.rows[0].count)) {
      res.status(400).send('Бронирование невозможно, существует другой заказ на этот период');
      return;
    }

    const orderDurationInDays = dayjs(endDate).diff(startDate, 'day') + 1;
  
    if (orderDurationInDays > 30) {
      res.status(400).send('Бронирование невозможно, слишком длинный период. Максимум 30 дней')
    }

    const newOrder = await client.query('INSERT INTO orders(car, startDate, endDate, price) VALUES($1, $2, $3, $4) RETURNING *', [car, toPGDate(startDate), toPGDate(endDate), calculatePrice(orderDurationInDays)])

    res.json(newOrder.rows[0])
  } catch (e) {
    next(e);
  }
}