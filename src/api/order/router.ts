import { Router } from 'express';
import { calculateOrderDetails, createOrder, getOrders } from './controller';

const orderRouter = Router();

orderRouter.get('/', getOrders);
orderRouter.post('/', createOrder);
orderRouter.get('/calculate', calculateOrderDetails);

export default orderRouter;
