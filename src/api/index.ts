import { Router } from 'express';
import carsRouter from './car/router';
import orderRouter from './order/router';

const router = Router();

router.use('/cars', carsRouter);
router.use('/orders', orderRouter);

export default router;