import { Client } from 'pg';

const client = new Client();

client.connect(err => console.log(err || 'connected to postgres'))

export default client;
