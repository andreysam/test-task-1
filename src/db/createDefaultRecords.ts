import defaultCars from './defaultCars.json';
import client from './index';

const insertCars = () => Promise.all(defaultCars.map(car => {
    return client.query(`INSERT INTO cars(brand, model, registration) VALUES($1, $2, $3)`, [car.brand, car.model, car.registration]).catch(() => true);
  }))

Promise.all([
  client.query(
    'CREATE TABLE cars ( \
      id uuid DEFAULT gen_random_uuid() PRIMARY KEY, \
      brand Char(50), \
      model Char(50), \
      registration Char(10) UNIQUE \
    )'
  ).then(insertCars).catch(insertCars),
  client.query(
    'CREATE TABLE orders ( \
      id uuid DEFAULT gen_random_uuid() PRIMARY KEY, \
      car uuid, \
      startDate date, \
      endDate date, \
      price int \
    )'
  ).catch(() => true)
]).then(() => {
  console.log('default records created')
})